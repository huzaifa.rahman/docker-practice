FROM ubuntu:20.04 AS build
RUN apt-get update && \
    apt-get install -y cmake && \
    apt-get install -y python python3 && \
    apt-get install -y libboost-all-dev git-all && \
    apt-get install -y build-essential && \
    rm -rf /var/lib/apt/lists/*     # used for storing information for each package resource specified in your system's sources
WORKDIR /src
COPY CMakeLists.txt main.cpp ./
RUN cmake . && make

FROM bitnami/minideb:stretch AS prod 
WORKDIR /opt/app
RUN mkdir results 
COPY --from=build /src/calculator ./
CMD ["./calculator"]