# Docker Practice

## Build image

- Use ``git clone https://gitlab.com/huzaifa.rahman/docker-practice.git`` to clone the repo. 
- Use ``docker-compose build`` command to build the image from ``Dockerfile`` in the parent directory.

## Run container

- Use ``docker-compose up`` command to run containers from the image created in the previous step.
- Place input in ``input.txt`` and the container will give the sqaure of this input in ``result`` file.

## Publishing image

- First tag the existing local image with ``docker tag <existing-image> <hub-user>/<repo-name>[:<tag>]``.
- Then push this new image with ``docker push <hub-user>/<repo-name>:<tag>``.
