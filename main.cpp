#include <iostream>
#include <string>
#include <fstream>
#include <unistd.h>

using namespace std;

int main()
{
    while (true)
    {
        ifstream input_file;
        ofstream output_file;
        string input;
        int input_int = 0;

        input_file.open("./results/input.txt");
        if (input_file.is_open())
        {
            input_file >> input;
            if (input.empty())
            {
                input_file.close();
                sleep(2);
                continue;
            }
            input_int = stoi(input);
        }
        input_file.close();

        output_file.open("./results/result");
        if(output_file.is_open())
        {
            output_file << input_int * input_int << endl;
        }
        output_file.close();
        
        sleep(2);
    }
    return 0;
}